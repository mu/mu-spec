#/usr/bin/env python3

"""
Extract comminst definitions from common-insts.rst into C macros.

USAGE: python3 script/comminsts-to-muapi.py
"""

import re
import sys

from muspecinjectablefiles import injectable_files, common_insts_path

pat = re.compile(r'\[(0x[0-9a-f]+)\]@([a-zA-Z0-9_.]+)', re.MULTILINE)

def main():
    defs = []
    longest = 0

    with open(common_insts_path) as f:
        text = f.read()

    for opcode, name in pat.findall(text):
        macro_name = "MU_CI_" + name.upper().replace(".", "_")
        opcode = opcode.lower()
        defs.append((name, macro_name, opcode))
        longest = max(longest, len(macro_name))

    lines = []

    for name, macro_name, opcode in defs:
        lines.append("#define {} ((MuCommInst){}) /// MUAPIPARSER muname:@{}".format(
            macro_name.ljust(longest), opcode, name))

    outtext = "\n".join(lines)

    injectable_files["muapi.h"].inject_many({
        "COMMINSTS": outtext,
        })

if __name__=="__main__":
    main()
