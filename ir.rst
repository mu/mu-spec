============================
Intermediate Representation
============================

Mu Intermediate Representation, or (Mu IR), is the language used by Mu to
represent a Mu program. It is the input from the client and can be executed on
Mu.

Mu can execute the program in any way, including interpretation, JIT compiling
or even Ahead-of-time compiling.

Mu IR is a tree-shaped structure that consists of nodes, including top-level
definitions and their children. It also has a human-readable text form.

This document describes the top-level of the Mu IR using the text form. There is
also the `IR Builder API <irbuilder.rst>`__, a programmatic interface to build
Mu IR inside a running micro VM.

There was a binary form, but is now deprecated. See `<ir-binary.rst>`__.

For the documents of the type system and the instruction set, see:

- `<type-system.rst>`__
- `<instruction-set.rst>`__

Example
=======

Here is an example of Mu IR in the text form::

    .typedef @i64 = int<64>
    .typedef @double = double
    .typedef @void = void
    .typedef @refvoid = ref<@void>

    .const @i64_0 <@i64> = 0
    .const @answer <@i64> = 42

    .typedef @some_global_data_t = struct <@i64 @double @refvoid>
    .global @some_global_data <@some_global_data_t>

    .typedef @Node = struct<@i64 @NodeRef>
    .typedef @NodeRef = ref<@Node>

    .funcsig @BinaryFunc = (@i64 @i64) -> (@i64)

    .funcdecl @square_sum <@BinaryFunc>

    .funcdef @gcd VERSION %v1 <@BinaryFunc> {
        %entry(<@i64> %a <@i64> %b):
            BRANCH %head(%a %b)

        %head(<@i64> %a <@i64> %b):
            %z = EQ <@i64> %b @i64_0
            BRANCH2 %z %exit(%a) %body(%a %b)

        %body(<@i64> %a <@i64> %b):
            %b1 = SREM <@i64> %a %b
            BRANCH %head(%b %b1)

        %exit(<@i64> %a):
            RET %a
    }

    .expose @gcd_native = @gcd #DEFAULT @i64_0

Later the client can submit a function that defines a previously undefined
function or a new version of a function that replaces the old version::

    .funcdef @square_sum VERSION %v1 <@BinaryFunc> {
        // define the function (if not defined)
    }

    .funcdef @gcd VERSION %v2 <@BinaryFunc> {
        // or replace an existing version (if already defined)
    }

Top-level Structure
===================

A **bundle** is the unit of code the client sends to Mu. It contains many
**top-level entities**. A top-level entity shall be a **type**, **function
signature**, **constant**, **global cell**, **function** or **exposed
function**.

Note that while *functions* are top-level, *function versions* are **not** top
level entities. Function versions are nodes under functions. The text form
allows writing ``.funcdef`` at the top level, but it is a syntax sugar of
defining a function (if not already existing) and also defining a version of it.

    NOTE: For Java users, a bundle is approximately the counterpart of a
    ``.class`` file.

In a bundle, top-level entities do not have any order. This means, in the text
form, top-level definitions can appear in any order and refer to each other by
names; in the bundle building API, top-level entities can be created in any
order using API functions and refer to each other symbolically.

Object Hierarchy
----------------

The object hierarchy (the "A has many B" or "A refers to many B" relation) of
the contents of a bundle is:

- A **bundle**

  - has 0-∞ **types**. Each type

    - refers to 0-∞ *types* or *function signatures*.

  - has 0-∞ **function signatures**. Each signature

    - refers to 0-∞ *types* (parameter types and return types)

  - has 0-∞ **constants**. Each constant

    - refers to 1 *type* (type of constant), and
      
    - 0-∞ other `global variables <instruction-set.rst#global-variable>`__
      (aggregated values, such as struct/array members).

  - has 0-∞ **global cells**. Each global cell

    - refers to 1 *type* (type of global cell).

  - has 0-∞ **exposed functions**. Each exposed function

    - refers to 1 *function* (the function to expose), and
    
    - refers to 1 *constant* (the `cookie <native-interface.rst#cookie>`__).

  - has 0-∞ **functions**. Each function

    - has 0-∞ **function versions**. Versions are ordered from oldest to newest
      in the order of bundle loading.  *If there is no version, the function is
      "undefined"; otherwise it is "defined".* Each version

      - has 1-∞ **basic blocks**. The first basic block is the *entry* block.
        Each block

        - has 0-∞ **normal parameters**. Each normal parameter
        
          - refers to 1 *type*.

        - has 0-1 **exceptional parameters**

        - has 1-∞ **instructions**. Each instruction

          - has 0-∞ results, and

          - may refer to 0-∞ other *types*, *function signatures* or *variables*
            (including both global and local variables).

Identifiers and Names
---------------------

Many entities in Mu are **identified**. The complete list of identified entities
are:

- Top-level entities, including:

  - types

  - function signatures

  - constants

  - global cells

  - functions

  - exposed functions

- Function versions

- Basic blocks

- Basic block parameters, including
  
  - normal parameters
    
  - exceptional parameters

- Instructions

- Instruction results

- Clauses of instructions, including:

  - destination clauses

  - exception clauses

  - keep-alive clauses

  - current-stack clauses

  - new-stack clauses

..

    **Notes to the micro VM implementers**: Although the bundle building API
    needs IDs of the above entities to construct IR nodes that refer to each
    other symbolically, recording the IDs and names for all of them may not be
    useful, and sometimes can consume a large amount of memory.
    
    In a steady state, the IDs and names of top-level entities need to be kept
    for building subsequent bundles that refer to them. The IDs and names of
    function versions and OSR-point instructions are needed for stack
    introspection. Other IDs and names are useless from the client's point of
    view, because the client cannot do anything with the IDs of those other
    entities, and the API does not require micro VM implementations to look up
    the IDs and names for the client.
    
    The micro VM implementers can make use of such restrictions on the API. So a
    space-efficient implementation does not need to record the IDs and names of
    **basic blocks**, **instruction results**, **non-OSR-point instrutions** or
    **clauses** of instructions. But recording them may provide more information
    for debugging.

An identified entity has an identifiers (ID) and **optionally** a name. An
identifier (ID) is a 32-bit integer.  A name is a string starting with a ``@``
or a ``%`` and followed by many characters in the set: ``[0-9a-zA-Z_-.]``. An ID
uniquely identifies an *identified* entity. A name, if present, also uniquely
identifies an *identified* entity.

IDs of entities are determined by the micro VM.

The text form Mu IR only refers to entities by names. When loaded into a micro
VM, the IDs of entities in a bundle is automatically generated.  When generating
IDs, Mu guarantees that **each name has a corresponding ID**, and **no two
different names are mapped to the same ID**.

Names
~~~~~

A **global name** begins with ``@``. All identified entities can use global
names. Top-level entities and function versions must use global names. Global
names are valid in a whole Mu instance, not limited to a single bundle.

    Example::

        .typedef    @i8 = int<8>
        .typedef    @i32 = int<32>
        .typedef    @i64 = int<64>
        .typedef    @ir_i8 = iref<@i8>
        .typedef    @ir_ir_i8 = iref<@ir_i8>

        .funcsig    @some_fun_sig = () -> ()
        .funcsig    @main_sig = (@i32 @ir_ir_i8) -> (@i32)

        .const      @i32_1 <@i32> = 1
        .const      @i64_0 <@i64> = 0

        .global     @errno <@i64>

        .funcdecl @some_fun <@some_fun_sig>

A **local name** begins with ``%``. Function versions, basic blocks, parameters
and instruction results may use local names in the IR.

Local names are a syntax sugar in the text-form IR. When parsed, they are
de-sugared into global names.

    NOTE: This implies that the client must use IDs or global names in the
    client API because there is no local name once a text-form bundle is loaded
    into the micro VM.

The global names are inferred from their syntactic parents:

- Within a function which has the global name ``@FuncGlobalName``, the function
  version ``%FV`` has global name ``@FuncGlobalName.FV``.

- Within a function version which has the global name ``@FuncVerGlobalName``, a
  basic block with local name ``%BB`` has global name ``@FuncVerGlobalName.BB``.

- Within a basic block which has the global name ``@BBGlobalName``, a parameter,
  an instruction or an instruction result with local name ``%LN`` has global
  name ``@BBGlobalName.LN``.

..

    Example::

        .funcsig @fac.sig = (... ...) -> (...)

        .funcdef @fac VERSION %v1 <@fac.sig> {
            %entry(<@i32> %n):
                [%first_br] BRANCH %head(%n @I32_1 @I32_1)

            %head(<@i32> %n <@i32> %p <@i32> %i):
                %lt = SLT <@i32> %i %n
                [%second_br] BRANCH2 %lt %body(%n %p %i) %exit(%p)

            %body(<@i32> %n <@i32> %p <@i32> %i):
                %p2 = MUL <@i32> %p %i
                %i2 = ADD <@i32> %i @I32_1
                BRANCH %head(%n %p2 %i2)

            %exit(<@i32> %p):
                RET %p
        }

    In the above example, the global names of the function version, the basic
    blocks and their parameters and instructions are:

    - ``%v1`` -> ``@fac.v1``

      - ``%entry`` -> ``@fac.v1.entry``

        - ``%n`` -> ``@fac.v1.entry.n``
        - ``%first_br`` -> ``@fac.v1.entry.first_br``

      - ``%head`` -> ``@fac.v1.head``

        - ``%n`` -> ``@fac.v1.head.n``
        - ``%p`` -> ``@fac.v1.head.p``
        - ``%i`` -> ``@fac.v1.head.i``
        - ``%lt`` -> ``@fac.v1.head.lt``
        - ``%second_br`` -> ``@fac.v1.head.second_br``

      - ``%body`` -> ``@fac.v1.body``

        - ``%n`` -> ``@fac.v1.body.n``
        - ``%p`` -> ``@fac.v1.body.p``
        - ``%i`` -> ``@fac.v1.body.i``
        - ``%p2`` -> ``@fac.v1.body.p2``
        - ``%i2`` -> ``@fac.v1.body.i2``
        - The anonymous ``BRANCH`` instruction: no global name.

      - ``%exit`` -> ``@fac.v1.exit``

        - ``%p`` -> ``@fac.v1.exit.p``
        - The anonymous ``RET`` instruction: no global name.

    Note that the same local name, such as ``%p``, has **different global names
    in different basic blocks**. ``@fac.v1.head.p`` and ``@fac.v1.body.p`` are
    not the same. They even seldom have the same value.

    Function versions, basic blocks, parameters and instructions can use global
    names, too. For example, instead of the previous example, it is legal to
    write::

        .funcdef @fac VERSION @fac.v1 <@fac.sig> {
            @fac.v1.entry(<@i32> @fac.v1.entry.n):
                BRANCH @fac.v1.head(<@i32> @fac.v1.entry.n <@i32> @I32_1 <@i32> @I32_1)

            @fac.v1.head(<@i32> @fac.v1.head.n <@i32> @fac.v1.head.p <@i32> @fac.v1.head.i):
                @fac.v1.head.lt = SLT <@i32> @fac.v1.head.i @fac.v1.head.n

    or even::

        .funcdef @fac VERSION @n1 <@fac.sig> {
            @n2(<@i32> @n3):
                BRANCH @n4(<@i32> @n3 <@i32> @I32_1 <@i32> @I32_1)

            @n4(<@i32> @n5 <@i32> @n6 <@i32> @n7)
                @n8 = SLT <@i32> @n7 @n5

Because local names are merely syntax sugar, everything that has a local name
can be identified by their global names. It is still considered a naming
conflict if two local names have the same global name.

..

    NOTE: It is useful to have local things globally identifiable, especially
    *function call sites* and *traps*. For example::

        .funcdef @foo VERSION @foo.v1 <...> {
            %entry():
                (%rv1 %rv2 %rv3) = [%the_call_site] CALL <@T1 @T2 @T3> @some_func (...)
                () = [%the_trap] TRAP <> KEEPALIVE (...)
                ...
        }

    The call site can be globally identified by ``@foo.v1.entry.the_call_site``.
    The trap can be identified by ``@foo.v1.entry.my_trap``. The name is
    globally unique and can be used to identify individual traps and call sites
    in *trap handlers* (see `the API <api.rst#trap-handling>`__).

Identifiers
~~~~~~~~~~~

All identifiers are global. Every ID uniquely identifies one entity in the whole
Mu instance.

0 is an invalid ID. IDs in the range of 1-65535 are reserved by Mu. The Mu
specification only uses 1-32767. 32768-65535 can be used by the Mu
implementation for extension purposes.

Type Definition
===============

Types and the **type constructor** syntax are documented in `<type-system.rst>`__.

A **type definition** gives a name to a type. It has the following form::

    .typedef Name = TypeCtor

where:

* ``Name`` is a global name for the type, and
* ``TypeCtor`` is a type constructor which defines the type.

In the bundle building API, type nodes are created using the ``new_type_*``
functions.

..

    Example: The following type definition defines a simple non-recursive type::

        .typedef @i64 = int<64>

    It gives a name ``@i64`` to a 64-bit integer.

..

    Example: The following type definition defines a recursive type::

        .typedef @i64 = int<64>
        .typedef @Node = struct<@i64 @NodeRef>
        .typedef @NodeRef = ref<@Node>

    These define a node in a singly-linked list. The second field of the struct
    is an object reference to itself. Note that **the order of top-level
    definitions does not matter**. They can be written in any order.

..

    NOTE: There is no way to simply make an alias of another type. ``.typedef
    @Foo = @Bar`` is illegal because ``@Bar`` is not a type constructor. In this
    event, replacing all occurrences of ``@Foo`` with ``@Bar`` in the whole
    program is the desired approach.

..

    For C programmers: In Mu IR, types cannot be "inlined", i.e. all types
    referenced by other definitions (such as other types, constants, globals,
    functions, ...) must be defined at top-level. For example::

        .typedef @refi64 = ref<int<64>>     // WRONG. Cannot write int<64> inside.
        
        .typedef @i64 = int<64>
        .typedef @refi64 = ref<@i64>        // Right.

        %sum = FADD <double> %a %b          // WRONG. "double" is a type constructor, not a type

        .typedef @double = double
        %sum = FADD <@double> %a %b         // Right.

Function Signature Definition
=============================

A **function signature definition** gives a name to a **function signature**, or
**signature** when unambiguous. It has the following form::

    .funcsig Name = SigCtor

where:

* ``Name`` is a global name for the signature, and
* ``SigCtor`` is a function signature constructor which defines the function
  signature.

A function signature constructor has the form::

    (ParamTys) -> (RetTys)

where both ``ParamTys`` and ``RetTys`` are a list of global names separated by
spaces for the types of parameters and return values, respectively.

In the bundle building API, function signatures are created by the
``new_funcsig`` function.

    Example: The following signature receives no parameters and return no
    values::

        .funcsig @empty_func_s = () -> ()

    The following signature receives a 64-bit integer and a double as parameters
    and returns an object reference to a 64-bit integer::

        .typedef @double = double
        .typedef @i64 = int<64>
        .typedef @refi64 = ref<@i64>
        .funcsig @some_func_s = (@i64 @double) -> (@refi64)

..

    For C programmers: Just like types, function signatures cannot be inlined.
    For example::

        // Asssume @i64 is defined as int<64>

        .typedef @foo_fp  = funcref<(@i64 @i64) -> (@i64)>      // WRONG.
        
        %rv = CALL <(@i64 @i64) -> (@i64)> @func (%arg1 %arg2)  // WRONG.

        .funcsig @foo_sig = (@i64 @i64) -> (@i64)
        .typedef @foo_fp  = funcref<@foo_sig>               // Right.

        %rv = CALL <@foo_sig> @func (%arg1 %arg2)           // Right.

Constant Definition
===================

Constants are global `SSA variables <instruction-set.rst#ssa-variables>`__ of
given values, can be used anywhere SSA variables are accepted, and cannot
change.

..

    For C programmers:

    1. In Mu, constants are just values, not part of the Mu memory. Although, in
       C, you can declare an object with the "const" decorator: ``const int a =
       42;`` and then have a pointer to it: ``const int* pa = &a;``, you cannot
       do the same in Mu. Global cells may be more appropriate if references to
       constants are ever taken.
    
    2. Just like types, constants cannot be inlined.  For example::

        // Asssume @i64 is defined as int<64>

        %b = ADD <@i64> %a 123          // WRONG. What type is 123?

        .const @I64_123 <@i64> = 123    // 123 is an @i64
        %b = ADD <@i64> %a @I64_123     // Right.

A **constant definition** has the form::

    .const Name <Type> = ConstCtor

where:

* ``Name`` is global name for the constant;
* ``Type`` is a global name for the type of the constant, and
* ``ConstCtor`` is a constant constructor.

A **constant constructor** can be the following:

- **integer constructor**, such as ``42``, ``0x2a``

- **floating point constructor**, such as ``3.14f``, ``3.14d``,
  ``bitsd(0x3ff8000000000000)``

- **list constructor**, such as ``{ @foo @bar @baz }``

- **null constructor**: ``NULL``

- **external constructor**, such as ``EXTERN "write"``

In the bundle building API, constant nodes are created using the ``new_const_*``
functions.

Integer Constructor
-------------------

**Integer constructors** are applicable to:

+ integer types: ``int<n>``, and
+ pointer types: ``uptr<T>``, ``ufuncptr<sig>``

The IR builder API constructs such constants using actual integers.

.. _integer-literal:

The text form is written as an **integer literal**, which is:
  
+ an optional sign [+-], followed by
+ an optional prefix: ``0`` or ``0x``, and
+ a sequence of digits [0-9a-fA-F].
  
A prefix 0 represents an octal number. A prefix 0x represents a hexadecimal
number. Otherwise it is a decimal number.

..

    NOTE: The client must ensure the number (integer or floating point) can be
    represented by the type, or it is an error.

..

    Example::
    
        .typedef @i64 = int<64>

        .const @oct1 <@i64> = 0
        .const @oct2 <@i64> = +01234567
        .const @dec1 <@i64> = 1234567890
        .const @hex1 <@i64> = -0x123456789abcdef0

        .typedef @ptri64          = uptr<@i64>
        .typedef @fpnoparamsnoret = ufuncptr<@noparamsnoret>

        // Address should be looked up before generating the bundle.
        .const @ptrconst <@ptri64> = 0x12345678
        .const @fpconst  <@fpnoparamsnoret> = 0x7ff00000000
        .const @nullptr  <@ptri64> = 0

Floating Point Constructor
--------------------------

**Floating point constructors** are applicable to floating point types:

+ ``float`` (if the suffix is 'f' in the text form);

+ ``double`` (if the suffix is 'd' in the text form).

The IR builder API constructs such constants using actual IEEE754 value.

The text form is written as a **floating point literal**, which can be one of
the following forms:

+ An optional sign [+-], an integral part, a dot (.), a fraction part,
  an optional exponent part and a suffix.

  * Both the integral part and the fraction part are a sequence of decimal
    digits [0-9].
  * The exponent part is ``e`` followed by an optional sign [+-] followed by a
    sequence of decimal digits [0-9].
  * The suffix is either ``f`` (for ``float``) or ``d`` (for ``double``).
  * Example: ``123.456f``, ``+123.456e789d``, ``-123.456e-789d``

+ One of ``nan``, ``+inf`` and ``-inf`` with a suffix ``f`` or ``d``.

  * Example: ``nanf``, ``-infd``

+ ``bitsf(intlit)`` or ``bitsd(intlit)`` where ``intlit`` is an `integer literal
  <integer-literal_>`__ as defined before, and the ``f`` and ``d`` represents
  ``float`` and ``double``, respectively. In the case, the resulting ``float``
  and ``double`` value has the same bit-wise representation as the 32-bit or
  64-bit integer of ``intlit``, respectively.

..

    Example::

        .typedef @float = float
        .typedef @double = double

        .const @float1 <@float> = 123.456f
        .const @float2 <@float> = +123.456e789f
        .const @float3 <@float> = -123.456e-789f
        .const @float4 <@float> = nanf
        .const @float5 <@float> = +inff
        .const @float6 <@float> = -inff
        .const @float7 <@float> = bitsf(0x7f800000)   // float inf
        .const @float7 <@float> = bitsf(0x7f800001)   // float nan
        
        .const @double1 <@double> = 123.456d
        .const @double2 <@double> = +123.456e789d
        .const @double3 <@double> = -123.456e-789d
        .const @double4 <@float> = nand
        .const @double5 <@float> = +infd
        .const @double6 <@float> = -infd
        .const @double7 <@float> = bitsd(0x7ff0000000000000)   // double inf
        .const @double7 <@float> = bitsd(0x7ff0000000000001)   // double nan

List Constructor
----------------

**List constructors** are applicable for composite types, but not hybrid:

+ ``struct``

+ ``array``

+ ``vector``

The IR builder API constructs such constants by providing a list of handles
to its members (other global SSA values).

In the text form, it is written as:

+ an opening brace ``{``, followed by

+ a sequence of global names of other global variables separated by spaces, and

+ a closing brace ``}``.

The sequence of names must have the same number of names as the number of
fields/elements as the type requires.

A constant must not recursively contain itself. (It is impossible to construct
recursive constants using the IR builder API.)

..

    Example::

        // simple struct
        .typedef @record_t = struct<@i64 @double>
        .const @record <@record_t> = {@dec1 @double1}

        // nested struct
        .typedef @nested_record_t = struct<@i64 @record_t @float>
        .const @nested_record <@nested_record_t> = {@hex1 @record @float2}

        // vector
        .typedef @4xfloat = vector <@float 4>
        .const @vec1 <@4xfloat> = { @float1 @float2 @float3 @float4 }

        // array constant
        // Not recommended to use unless intracting with native function.
        .typedef @i64ary = array<@i64 3>
        .const @constary <@i64ary> = {@oct1 @oct2 @dec1}

        // Global cells and functions are global variables, too.
        // They can be components of constants.
        .global @g1 <@i64>

        .funcsig @noparamsnoret = () -> ()
        .funcdecl @f1 <@noparamsnoret>

        .typedef @irefi64 = iref<@i64>
        .typedef @record2_t = struct<@irefi64 @some_func>

        .const @record2 <@record2_t> = {@g1 @f1}

Null constructor
----------------

**Null constructors** are applicable for all *general reference types* except
`weakref <type-system.rst#reference-types>`__:

- ``ref<T>``

- ``iref<T>``

- ``funcref<sig>``

- ``threadref``

- ``stackref``

- ``framecursorref``

- ``irbuilderref``

It can only have ``NULL`` value.

In the text form, it is written as the **null literal**: ``NULL``. 

..

    NOTE: ``weakref`` cannot be the type of an SSA variable, but all constants
    are SSA variables. (See `<type-system.rst>`__ and `<instruction-set.rst>`__).

..

    NOTE: The only constant of reference types is ``NULL``. The reason why Mu
    forbids constant object references is manifold:

    * To define a constant heap reference, the client must provide a reference
      to a heap object, which itself is recursively a constant heap reference.
      Even if such a reference is created, it renders a heap object immortal (as
      immortal as a global cell), which defeated the purpose of garbage
      collection.

    * When a heap object is moved, the garbage collector must update all
      existing references to the object. This makes the constant reference not
      really "constant". Extra difficulties are introduced when such references
      become immediate values in the machine code.

    The global memory is an alternative to such needs. Either store a global
    data structure in the global memory or allocate it in the heap and assign
    its reference to a global cell. In fact, the ID or the name of any global
    cell is a constant SSA variable of an internal reference to it. The ID or
    the name of a Mu function is a constant SSA variable of a ``funcref``.

..

    Example::

        .typedef @void = void
        .typedef @ref_void = ref<@void>
        .const @null_ref <@ref_void> = NULL

        .typedef @iref_void = iref<@void>
        .const @null_iref <@iref_void> = NULL

        .typedef @some_func = funcref<@noparamsnoret>
        .const @null_func <@some_func> = NULL

        .typedef @tref = threadref
        .const @null_tr <@tref> = NULL

        .typedef @sref = stackref
        .const @null_sr <@sref> = NULL

External Constructor
--------------------

**External constructor** are applicable for pointer types:

- ``uptr<T>``

- ``ufuncptr<T>``

In the text form, it is written as:

+ the keyword ``EXTERN``, followed by

+ a string literal, which is a sequence of ASCII characters surrounded by ``"``
  (code is 34).  The code of each character shall be within 33–126 inclusive,
  but not 34 (non-space printable characters except ``"``). There are no escape
  sequences. This string represents a symbolic name.

The IR builder API constructs such constants by providing the symbol, which has
the same limitation as the text form: 33-126 but not 34.

The values of such constants are implementation-defined. Usually the
implementation will resolve the symbolic names to the address of C functions.

..

    Example::

        .typedef @char    = int<8>
        .typedef @charp   = uptr<@char>
        .typedef @int     = int<32>
        .typedef @void    = void
        .typedef @voidp   = uptr<@void>
        .typedef @size_t  = int<64>
        .typedef @ssize_t = int<64>

        .funcsig @write.sig = (@int @voidp @size_t) -> (@ssize_t)
        .typedef @write.fp  = ufuncptr<@write.sig>

        .const @write = EXTERN "write"

        .funcsig @puts.sig = (@charp) -> (@int)
        .typedef @puts.fp  = ufuncptr<@puts.sig>

        .const @puts = EXTERN "puts"

        .funcdef @main ... <...> {
            %...(...):
                ...
                %rv = CCALL #DEFAULT <@write.fp @write.sig> @write (%fd %buf %sz)
                ...
        }

Global Cell Definition
======================

A **global cell definition** defines a **global cell**. A global cell is the
memory allocation unit in the *global memory*. See `<memory.rst>`__ for more
information about the global memory.

    NOTE: The global memory is the counterpart of static or global variables in
    C/C++. In Mu, global cells are also permanently pinned so that it can be
    used to interact with native programs.

A global cell definition has the form::

    .global Name <Type>

* where ``Name`` is a global name for the global cell and
* ``Type`` is a global name for the type of the data the global cell
  represents.

In the bundle building API, global cell nodes are created using the
``new_global_cell`` function.

..

    Example::

        .typedef    @i8 = int<8>
        .typedef    @i32 = int<32>
        .const      @i32_0 <@i32> = 0

        .global @my_errno <@i32>

        .typedef @small_char_array = array<@i8 12>
        .global @hello_world_str <@small_char_array>
        
        // The client can populate the memory in @hello_world_str at loading time

        .funcdef @func VERSION ... <...> {
            %entry():
                %a = LOAD <@i32> @my_errno       // @my_errno has type iref<@i32>
                STORE <@i32> @my_errno @i32_0
                ...
        }

..

    For C programmers: Unlike C, global cells cannot be initialised as C global
    variables. Mu global cells (as any Mu memory locations) are initialised to 0
    or NULL. Writing to global cells can only be done via memory accessing
    (load, store, ...), or indirectly via the `HAIL <hail.rst>`__ language.
    Beware that concurrent non-atomic access (even as a result of careless
    initialisation) may result in data race, which has undefined behaviour.

Functions and Function Versions
===============================

**Functions** are callable in Mu. A function has a signature, which determines
its parameter types and return types.

A function may have zero or more *versions*. A function is said to be
"undefined" if it has zero versions; otherwise it is "defined".

A **version of function** has its control flow graph which defines how it is
executed. Function versions cannot be directly called. Calling a *function* will
actually call the most recent version a thread can see (see `below
<#func-exec>`__), or cause a trap if undefined. All versions of a function must
have the same signature.

Creating a Function Without Versions (Undefined Function)
---------------------------------------------------------

In the text form, the top-level **function declaration** creates a function with
no versions. It has the following form::

    .funcdecl Name <Sig>

where:

* ``Name`` is a global name for the function and
* ``Sig`` is a global name for the signature of the function.

    FIXME: The term "function declaration" is misleading because it is not the
    same as the function declaration in C. It should be replaced with simple and
    explicit operations such as "create functions" and "create function
    versions" in future Mu designs. For example::

        .func Name <Sig>

        .funcver VerName DEFINES FuncName {
            %entry(...):
            ...
        }

In the bundle building API, A function node is created using the `new_func
<irbuilder.rst#new-func>`__ function.

It is an error to create two functions of the same name/ID. It is considered a
conflict.

    NOTE for C programmers: If a function is already created in one bundle, it
    is already visible (as already created previously) in subsequent bundles,
    and does not need to be created again. This is different from the C model,
    where each compilation unit "declares" functions which are defined in other
    compilation units, and are resolved at link time. C does not address the
    order of bundle loading, but in Mu, the order matters because it determines
    the order of function redefinition.

..

    Example::

        .typedef @i64 = int<64>
        .typedef @float = float
        .typedef @double = double

        .funcsig @ExampleSig = (@float @double) -> (@i64)

        .funcdecl @example <@ExampleSig>

Creating a Function With a Version (Defined Function)
-----------------------------------------------------

In the text form, the top-level **function definition** creates a function
version of a given function. If the function is not yet created, *function
definition* will implicitly define the *function*. The text form syntax is::

    .funcdef Name VERSION VerName <Sig> { Body }

where:

* ``Name`` is a global name for the function
* ``VerName`` is a global name of this particular version of function
* ``Sig`` is a global name for the signature of the function
* ``Body`` is a sequence of instructions, constants and labels.

In the `bundle building API <irbuilder.rst#define-a-function>`__, *functions*
and *function versions* are created separately.

The function itself is created by the ``new_func`` function. Then a function
version is created using the ``new_func_ver`` function. Basic blocks and
instructions are created using their respective functions. See the `API
<irbuilder.rst#define-a-function>`__.

It is an error to create two versions of the same function in one bundle.

    NOTE: Bundle is the unit of loading. Bundle loading has the side effect of
    re-defining existing functions. If a bundle has a new version of an existing
    function, the new version will become the newest version when the bundle is
    loaded. So it does not make sense to have two versions of the same function
    in the same bundle: which new version should Mu redefine the old function
    to?

..

    Example::

        .typedef @i64 = int<64>
        .typedef @float = float
        .typedef @double = double

        .funcsig @ExampleSig = (@float @double) -> (@i64)

        .funcdef @example VERSION %v1 <@ExampleSig> {
            ...
        }

.. _func-exec:

Semantics of Function Calls
---------------------------

When a thread executes a *function*, a **conceptual** lookup is performed to
find the latest version of a function with respect to the current thread (see
`memory model <memory-model.rst#special-funcdef>`__). If the version is found,
the thread executes that function from the entry block.

    NOTE: The lookup is conceptual because it does not have to be implemented as
    an actual indirection. When Mu is implemented as a native code compiler,
    "redefinition" can be implemented as "patching the first instruction of all
    old function versions into a JMP instruction to the newest version". By
    doing this, all call instructions to old versions will be redirected to the
    newest version.

    ``funcref`` can be implemented as direct function pointers to the latest
    version. When function redefinition happens, some ``funcref`` will still
    point to the old version, but this does not affect the correctness of
    function calls. ``funcref`` can be updated at GC time using the same
    mechanism as object movement.

    Static call sites may use the compiled function version's address as
    immediate operands. These instructions can be patched at any appropriate
    time.

If the version is not found (executing undefined function), it behaves as if it
has a hidden version defined as::

    .funcdef Name VERSION NoVersion <Sig> {
        %entry(ParamList):
            TRAP <> KEEPALIVE (ParamList)
            TAILCALL <Sig> Name (ParamList)
    }

That is, it will trap to the client, using all parameters as the keep-alive
variables. If the stack is ever rebound, passing no values, it will try to
tail-call the same function (**Not necessarily the same hidden version!** It may
have been defined by the client in the trap! Clients that support lazy loading
are very likely to do so.) using the same arguments. If an exception is thrown
when rebound, the ``TRAP`` in this hidden version will re-throw it to the parent
frame. The ``cur_func`` API will return the ID of the function.  This hidden
version is still not a real version, so the ``cur_func_ver`` API function will
return 0.  The ``TRAP`` is not a real instruction, either, so the ``cur_inst``
API function will also return 0.  ``dump_keepalives`` will dump the arguments.


See the `Bundle Loading`_ section for the semantics of bundle loading and
function redefinition.

.. _exception-parameter:

Function Body
=============

A function definition has a **function body**.

A function body has many **basic block** enclosed between ``{`` and ``}``. A
basic block has the following form::

    Name (ParamList) ExcParam:
        Inst
        Inst
        ...

where ``ParamList`` is a list of type-name pairs: ``<T1> N1 <T2> N2 <T3> N3
...``, which specifies the normal **parameters** to the basic block.
``ExcParam`` is optional.  When present, it has the form ``[ ExcName ]`` where
``ExcName`` is the name of the **exceptional parameter**, which is also a
(non-normal) parameter. The exceptional parameter always has the ``ref<void>``
type. Many instructions follow the colon ``:``.

    Example::

        %bb1(<@T1> %p1 <@T2> %p2 <@T3> %p3):
            ...

        %bb2() [%exc]:
            ...

The first basic block is the **entry block**. The parameters of the entry block
must match the parameters of the function (determined by its signature).  The
execution starts from the entry block, and its parameters receive the arguments
to the function. The entry block must not have the exceptional parameter.

..

    NOTE: The name of the entry block is conventionally called ``%entry``, but
    is not compulsory.

The entry block must not be branched to from any basic blocks. Other basic
blocks are executed when branched to. The normal parameters receive arguments
from the branching sites. The exceptional parameter receives the exception
caught by the branching site as the argument. If the exceptional parameter is
omitted but the basic block is supposed to receive an exception, the exception
will be silently ignored. A basic block with an exceptional parameter must only
be used as the exceptional destination of instructions which can catch
exceptions, which currently include ``CALL``, ``TRAP``, ``WATCHPOINT`` and
``SWAPSTACK``.

    An example of a basic block with an exceptional parameter::

        %entry():
            ...
            %rv = CALL <...> @foo (...) EXC(%nor_dest(%rv) %exc_dest(%a %b %c))

        %nor_dest(<@T0> %p0):
            // process the return value
            // %p0 = %rv

        %exc_dest(<@T1> %p1 <@T2> %p2 <@T3> %p3) [%exc]:
            // handle exceptions here
            // %p1 = %a, %p2 = %b, %p3 = c, %exc = the exception

Each basic block contains a sequence of **instructions**. An instruction has one
of the following forms::

    ( Name1 Name2 Name3 ... ) = InstName InstBody

    Name = InstName InstBody

    InstName InstBody

The left hand side can be a list of names: ``( Name1 Name2 Name3 ... )``, each
is bound to a result of an instruction.

The latter two forms are syntax sugars. A single name without brackets is a
syntax sugar of ``( Name )``. If both the name and the equal sign ``=`` are
omitted, it is equivalent to an empty list of names: ``()``.

The number of results written in the IR must match the actual number of results
the instruction produces.

The ``InstName`` is optional. When present, it has the form ``[ Name ]``, where
``Name`` is the name of the instruction. This can be used to identify a
particular instruction, especially ``CALL`` and ``TRAP``. If ``InstName`` is
absent, the instruction does not have a name.

    Examples:

    ``%call_site`` is the name of the instruction; ``%rv1``, ``%rv2`` and
    ``%rv3`` are the names of return values::

        (%rv1 %rv2 %rv3) = [%call_site] CALL <@sig> @callee (%a1 %a2)

    The instruction has no name; ``%rv`` is the return value::

        %rv = ADD <@i64> %x %y

    ``%trap1`` is the name of the instruction; There is no return values. The
    empty angular bracket ``<>`` means the TRAP is not expecting any return
    values::

        [%trap1] TRAP <> KEEPALIVE (%x %y %z)


The grammar of the ``InstBody`` part for each instruction is defined separately
in `<instruction-set.rst>`__.

    Full example::

        .typedef @i64 = int<64>
        .funcsig @gcd_sig = (@i64 @i64) -> (@i64)

        .const @i64_0 <@i64> = 0

        .funcdef @gcd VERSION %v1 <@gcd_sig> {
            %entry(<@i64> %a <@i64> %b):
                BRANCH %head(%a %b)

            %head(<@i64> %a <@i64> %b):
                %z = EQ <@i64> %b @i64_0
                BRANCH2 %z %exit(%a) %body(%a %b)

            %body(<@i64> %a <@i64> %b):
                %b1 = SREM <@i64> %a %b
                BRANCH %head(%b %b1)

            %exit(<@i64> %a):
                RET %a
        }

The last instruction of any basic block must be a **terminator instruction**,
which is one of the following:

- ``BRANCH``, ``BRANCH2``, ``SWITCH``, ``WPBRANCH``
- ``TAILCALL``
- ``RET``
- ``THROW``
- ``SWAPSTACK`` if the "current stack clause" is ``KILL_OLD``
- Some `Common Instructions <common-insts.rst>`__ are always terminators:

  - ``@uvm.thread_exit``

- Any instructions that may have an **exception clause** and actually have the
  exception clause, which are:

  - Binary operations (only ``UDIV``, ``SDIV``, ``UREM`` and ``SREM``)
  - ``CALL``, ``CCALL``
  - ``NEW``, ``NEWHYBRID``, ``ALLOCA``, ``ALLOCAHYBRID``
  - ``LOAD``, ``STORE``, ``CMPXCHG``, ``ATOMICRMW``
  - ``TRAP``, ``WATCHPOINT``
  - ``NEWTHREAD``, ``SWAPSTACK``
  - Some `Common Instructions <common-insts.rst>`__ when having exception
    clause

    - ``@uvm.new_stack``

..

    NOTE: This is to say, for example, if a particular ``CALL`` instruction does
    have an exception clause, then it is a terminator. If it does not have
    exceptional clause clause, it is not a terminator.

Function Exposing Definition
============================

A **function exposing definition** creates a value (usually an untraced function
pointer, ``ufuncptr``) so that the Mu function can be called from native
programs (usually C programs).

In the text form, it has the following form::

    .expose Name = FuncName CallConv Cookie

where:

* ``Name`` is a global name of the exposed function.

* ``FuncName`` is the name of a Mu function.

* ``CallConv`` is a flag that denotes the calling convention. See platform-specific ABI.

* ``Cookie`` is the cookie. Must be the global name to a ``int<64>`` constant.

This definition exposes a Mu function *FuncName* as an exposed value, identified
by *Name*, using the calling convention *CallConv*.

The *Cookie* is an ``int<64>`` constant attached to this exposed value. See
`Native Interface <native-interface.rst#cookie>`__ for more explanations.

    NOTE: It is the **function** that is exposed, not the version. Like in Mu, C
    programs can only directly call Mu functions rather than function versions.
    When a Mu function is redefined, the exposed function will automatically
    use the new version if called after redefining.

How such an exposed function can be called is implementation-specific.

    NOTE: The spec is just trying to be general. In most cases, it will be
    called from C, and the exposed function is seen by C as a function pointer,
    and can be called using the same C calling convention (usually
    ``#DEFAULT``).

    Example::

        .expose @name = @func #DEFAULT @cookie

Bundle Loading
==============

The API provides the `load_bundle <api.rst#bundle-and-hail-loading>`__ and the
`IRBuilder.load <irbuilder.rst#load>`__ functions. These functions load bundles
from the text form or the constructed AST. They can be can be called by multiple
client threads on their `client contexts <api.rst#client-context>`__, and the
result is always equivalent to as if they were loaded in a particular sequence.

The client must ensure the names of all entities in all bundles (already loaded
or being loaded) are distinct.

    NOTE: There is a special case for the text form: If the function name in the
    ``.funcdef`` definition (such as ``@f`` in ``.funcdef @f VERSION @v <@sig> {
    ... }``) is the same as a function already loaded, it only defines a new
    version ``@v`` for the existing function ``@f``. If the function ``@f`` does
    not exist, the text form creates both a new function ``@f`` and a new
    version ``@v``.

If a bundle contains a new version of an existing function, it **redefines** the
function. After this bundle is loaded, all function-calling operations to the
function that `happen after <memory-model.rst#happens-before>`__ the bundle
loading operation will call the newly defined version of the function.  The
actions of defining functions (bundle loading) and using of functions (including
function calls and the creation of stacks, i.e. the `@uvm.new_stack
<common-insts.rst#uvm-new-stack>`__ instruction or the `new_stack
<api.rst#new-stack>`__ API) obey the memory model of the ``RELAXED`` order as if
the definition is a store and the use is a load.  See `Memory Model
<memory-model.rst#special-funcdef>`__.

All existing activations of any functions remain unchanged, that is, they remain
to be the old versions of the functions. 

    NOTE: Specifically, existing `traps (including watchpoints)
    <instruction-set.rst#traps-and-watchpoints>`__ in older versions of
    functions remain valid. During OSR, redefining a function will not affect
    any existing function activations unless they are explicitly popped by the
    client.

.. vim: textwidth=80
