===================
Common Instructions
===================

This document specifies Common Instructions.

**Common Instructions** (sometimes abbreviated as "comminst") are instructions
that have a common format and are used with the ``COMMINST`` super instruction.
They have:

1. An ID and a name. (This means, they are *identified*. See `<ir.rst>`__.)
2. A flag list.
3. A type parameter list.
4. A value parameter list.
5. An optional exception clause.
6. A possibly empty (which means optional) keep-alive clause.

*Common instructions* are a mechanism to extend the Mu IR without adding new
instructions or changing the grammar.

    NOTE: *Common instructions* were named "intrinsic function" in previous
    versions of this document. The name was borrowed from the LLVM. However, the
    common instructions in Mu are quite different from the usual concept of
    intrinsic functions.

    Intrinsic functions usually mean a kind a function that is understood
    directly by the compiler. The C function ``memcpy`` is considered an
    intrinsic function by some compilers. In JikesRVM, methods of the ``Magic``
    class are a kind of intrinsic functions. They appear like ordinary functions
    in the language and bypass all front-end tools including the C parser and
    javac, but they are understood by the backend. Their purpose is to perform
    tasks that cannot be expressed by the high-level programming language,
    including direct raw memory access in Java.

    Common instructions only differ from ordinary Mu instructions in that they
    have a common format and are called by the ``COMMINST`` super instruction.
    The purpose is to add more instructions to the Mu IR without having to
    modify the parser.

    Common instructions are not Mu functions and cannot be called by the
    ``CALL`` instruction, nor can it be directly used from the high-level
    language that the client implements. The Mu client must understand common
    instructions because it is the only source of IR code of Mu. That is to say,
    *there is no way any higher-level program can express anything which Mu
    knows but the client does not*. For special high-level language functions
    that cannot be directly implemented in the high-level programming language,
    like the methods in the ``java.lang.Thread`` class, the client must
    implement those special high-level language functions in "ordinary" Mu IR
    code, which may or may not involve common instructions. For example,
    creating a thread is a "magic" in Java, but it is not more special than
    executing an instruction (``NEWTHREAD``) in Mu. Some Java libraries require
    Mu to make a ``CCALL`` to some C functions which are provided by the JVM,
    and they slip under the level of Mu. But Mu and the client always know the
    fact that "it call C function" and it is not magic.

This document uses the following notation::

    [id]@name [F1 F2 ...] < T1 T2 ... > <[ sig1 sig2 ... ]> ( p1:t1, p2:t2, ... ) excClause KEEPALIVE -> RTs
    
- ``id`` is the ID and ``@name`` is the name.

- ``[F1 F2 ...]`` is a list of flag parameters.

- ``[T1 T2 ...]`` is a list of type parameters. The users pass types into the
  common instruction via this list.

- ``<[ sig1 sig2 ... ]>`` is a list of function signature parameters.

- ``(p1:t1, p2:t2, ...)`` is a list of pairs of symbolic name and type.  It is
  the value parameter list with the type of each parameter. The user only passes
  values via this list, and the types are only parts of the documentation.

If any of the above list is omitted in this document, it means the respective
common instruction does not take that kind of parameters.

If ``excClause`` or ``KEEPALIVE`` are present, they mean that the common
instruction accepts exception clause or keepalive clause, respectively.
Otherwise the common instruction does not branch to exception destinations nor
support any keep-alive variables.

``RTs`` are the return types. If the return type is omitted, it means it
produces no results (equivalent to ``-> ()``).

The names of many common instructions are grouped by prefixes, such as
``@uvm.tr64.``. In this document, their common prefixes may be omitted in their
descriptions when unambiguous.

Thread and Stack operations
===========================

.. _uvm-new-stack:

::

    [0x201]@uvm.new_stack <[sig]> (%func: funcref<sig>) -> stackref

Create a new stack with ``%func`` as the stack-bottom function. ``%func`` must
have signature ``sig``. Returns the stack reference to the new stack.

The stack-bottom frame is in the state **READY<Ts>**, where *Ts* are the
parameter types of ``%func``.

This instruction continues exceptionally if Mu failed to create the stack. The
exceptional parameter receives NULL.

::

    [0x202]@uvm.kill_stack (%s: stackref)

Destroy the given stack ``%s``. The stack ``%s`` must be in the **READY** state
and will enter the **DEAD** state.

::

    [0x203]@uvm.thread_exit

Stop the current thread and kill the current stack. The current stack will enter
the **DEAD** state. The current thread stops running.

::

    [0x204]@uvm.current_stack -> stackref

Return the current stack.

::

    [0x205]@uvm.set_threadlocal (%ref: ref<void>)

Set the thread-local object reference of the current thread to ``%ref``.

::

    [0x206]@uvm.get_threadlocal -> ref<void>

Return the current thread-local object reference of the current thread.

64-bit Tagged Reference
=======================

::

    [0x211]@uvm.tr64.is_fp  (%tr: tagref64) -> int<1>
    [0x212]@uvm.tr64.is_int (%tr: tagref64) -> int<1>
    [0x213]@uvm.tr64.is_ref (%tr: tagref64) -> int<1>

- ``is_fp`` checks if ``%tr`` holds an FP number.
- ``is_int`` checks if ``%tr`` holds an integer.
- ``is_ref`` checks if ``%tr`` holds a reference.

Return 1 or 0 for true or false.

::

    [0x214]@uvm.tr64.from_fp  (%val: double) -> tagref64
    [0x215]@uvm.tr64.from_int (%val: int<52>) -> tagref64
    [0x216]@uvm.tr64.from_ref (%ref: ref<void>, %tag: int<6>) -> tagref6

- ``from_fp``  creates a ``tagref64`` value from an FP number ``%val``.
- ``from_int`` creates a ``tagref64`` value from an integer ``%val``.
- ``from_ref`` creates a ``tagref64`` value from a reference ``%ref`` and the
  integer tag ``%tag``.

Return the created ``tagref64`` value.


::

    [0x217]@uvm.tr64.to_fp  (%tr: tagref64) -> double
    [0x218]@uvm.tr64.to_int (%tr: tagref64) -> int<52>
    [0x219]@uvm.tr64.to_ref (%tr: tagref64) -> ref<void>
    [0x21a]@uvm.tr64.to_tag (%tr: tagref64) -> int<6>

- ``to_fp``  returns the FP number held by ``%tr``.
- ``to_int`` returns the integer held by ``%tr``.
- ``to_ref`` returns the reference held by ``%tr``.
- ``to_tag`` returns the integer tag held by ``%tr`` that accompanies the
  reference.

They have undefined behaviours if ``%tr`` does not hold the value of the
expected type.

Math Instructions
=================

    TODO: Should provide enough math functions to support:

    1. Ordinary arithmetic and logical operations that throw exceptions when
       overflow. Example: C# in checked mode, ``java.lang.Math.addOvf`` added in
       Java 1.8.
    2. Floating point math functions. Example: trigonometric functions, testing
       NaN, fused multiply-add, ...

    It requires some work to decide a complete list of such functions. To work
    around the limitations for now, please call native functions in libc or
    libm using ``CCALL``.

Futex Instructions
==================

See `<threads-stacks.rst>`__ for high-level descriptions about Futex.

Wait
----

::

    [0x220]@uvm.futex.wait <T> (%loc: iref<T>, %val: T) -> int<32>
    [0x221]@uvm.futex.wait_timeout <T> (%loc: iref<T>, %val: T, %timeout: int<64>) -> int<32>

``T`` must be an integer type.

``wait`` and ``wait_timeout`` verify if the memory location ``%loc`` still
contains the value ``%val`` and then put the current thread to the waiting queue
of memory location ``%loc``. If ``%loc`` does not contain ``%val``, return
immediately. These instructions are atomic.

- ``wait`` waits indefinitely.

- ``wait_timeout`` has an extra ``%timeout`` parameter which is a 64-bit
  unsigned integer that represents a time in nanoseconds. It specifies the
  duration of the wait.

Both instructions are allowed to spuriously wake up.

They return a signed integer which indicates the result of this call:

* 0: the current thread is woken.
* -1: the memory location ``%loc`` does not contain the value ``%val``.
* -2: spurious wakeup.
* -3: timeout during waiting (``wait_timeout`` only).

Wake
----

::

    [0x222]@uvm.futex.wake <T> (%loc: iref<T>, %nthread: int<32>) -> int<32>

``T`` must be an integer type.

``wake`` wakes *N* threads in the waiting queue of the memory location ``%loc``.
This instruction is atomic.

*N* is the minimum value of ``%nthread`` and the actual number of threads in the
waiting queue of ``%loc``. ``%nthread`` is signed. Negative ``%nthread`` has
undefined behaviour.

It returns the number of threads woken up.

Requeue
-------

::

    [0x223]@uvm.futex.cmp_requeue <T> (%loc_src: iref<T>, %loc_dst: iref<T>, %expected: T, %nthread: int<32>) -> int<32>

``T`` must be an integer type.

``cmp_requeue`` verifies if the memory location ``%loc_src`` still contains the
value ``%expected`` and then wakes up *N* threads from the waiting queue of
``%loc_src`` and move all other threads in the waiting queue of ``%loc_src`` to
the waiting queue of ``%loc_dst``. If ``%loc_src`` does not contain the value
``%expected``, return immediately. This instruction is atomic.

*N* is the minimum value of ``%nthread`` and the actual number of threads in the
waiting queue of ``%loc``. ``%nthread`` is signed. Negative ``%nthread`` has
undefined behaviour.

It returns a signed integer. When the ``%loc_src`` contains the value of
``%expected``, return the number of threads woken up; otherwise return -1.

Miscellaneous Instructions
==========================

::

    [0x230]@uvm.kill_dependency <T> (%val: T) -> T

Return the same value as ``%val``, but ``%val`` does not carry a dependency to
the return value.

    NOTE: This is supposed to free the compiler from keeping dependencies in
    some performance-critical cases.

Native Interface
================

.. _pinning:

.. _get-addr:

Object pinning
--------------

::

    [0x240]@uvm.native.pin      <T> (%opnd: T) -> uptr<U>
    [0x241]@uvm.native.unpin    <T> (%opnd: T)
    [0x242]@uvm.native.get_addr <T> (%opnd: T) -> uptr<U>

*T* must be ``ref<U>`` or ``iref<U>`` for some U.

- ``pin`` adds one instance of the reference ``%opnd`` to the pinning multiset
  of the current thread.  Returns the mapped pointer to the bytes for the memory
  location.  If *T* is ``ref<U>``, it is equivalent to pinning the memory
  location of the whole object (as returned by the ``GETIREF`` instruction). If
  *opnd* is ``NULL``, the result is a null pointer whose address is 0.

- ``unpin`` removes one instance of the reference ``%opnd`` from the pinning
  multiset of the current thread. It has undefined behaviour if no such an
  instance exists.

- ``get_addr`` gets the address of a memory location ``%opnd`` which must
  already be pinned with respect to the current thread. This include all global
  cells and other memory locations in the pinning set of the current thread.

Mu function exposing
--------------------

::

    [0x243]@uvm.native.expose [callconv] <[sig]> (%func: funcref<sig>, %cookie: int<64>) -> U

*callconv* is a platform-specific calling convention flag. *U* is determined by
the calling convention and *sig*.

``expose`` exposes a Mu function *func* as a value according to the calling
convention *callConv* with cookie *cookie*.

    Example::
        
        .funcdef @foo VERSION ... <@foo_sig> (...) { ... }

        %ev = COMMINST @uvm.native.expose [#DEFAULT] <[@foo_sig]>

::

    [0x244]@uvm.native.unexpose [callconv] (%value: U)

*callconv* is a platform-specific calling convention flag. *U* is determined by
the calling convention.

``unexpose`` removes the exposed value.

::

    [0x245]@uvm.native.get_cookie () -> int<64>

If a Mu function is called via its exposed value, this instruction returns the
attached cookie. Otherwise it returns an arbitrary value.

Metacircular Client Interface
=============================

These are additional instructions that enables Mu IR programs to behave like a
client.

Some types and signatures are pre-defined. They are always available. Note that
the following are not strict text IR syntax because some types are defined in
line::

    .typedef @uvm.meta.bytes   = hybrid <int<64> int<8>>    // ID: 0x260
    .typedef @uvm.meta.bytes.r = ref<@uvm.meta.bytes.r>     // ID: 0x261
    .typedef @uvm.meta.refs    = hybrid <int<64> ref<void>> // ID: 0x262
    .typedef @uvm.meta.refs.r  = ref<@uvm.meta.refs.r>      // ID: 0x263

    .funcsig @uvm.meta.trap_handler.sig       = (stackref int<32> ref<void>) -> ()   // ID: 0x264

In ``bytes`` and ``refs``, the fixed part is the length of the variable part.
``bytes`` represents a byte array. ASCII strings are also represented this way.

ID/name conversion
------------------

::

    [0x250]@uvm.meta.id_of (%name: @uvm.meta.bytes.r) -> int<32>
    [0x251]@uvm.meta.name_of (%id: int<32>) -> @uvm.meta.bytes.r

- ``id_of`` converts a textual Mu name ``%name`` to the numerical ID. The name
  must be a global name.

- ``name_of`` converts the ID ``%id`` to its corresponding name. If the name
  does not exist, it returns ``NULL``. The returned object must not be modified.

They have undefined behaviours if the name or the ID in the argument do not
exist, or ``%name`` is ``NULL``.

Bundle/HAIL loading
-------------------

::

    [0x252]@uvm.meta.load_bundle (%buf: @uvm.meta.bytes.r)
    [0x253]@uvm.meta.load_hail   (%buf: @uvm.meta.bytes.r)

``load_bundle`` and ``load_hail`` loads Mu IR bundles and HAIL scripts,
respectively. ``%buf`` is the content.

    TODO: These comminsts should be made optional, and the IR Builder API should
    be provided as comminsts, too.

Stack introspection
-------------------

::

    [0x254]@uvm.meta.new_cursor         (%stack: stackref) -> framecursorref
    [0x255]@uvm.meta.next_frame         (%cursor: framecursorref)
    [0x256]@uvm.meta.copy_cursor        (%cursor: framecursorref) -> framecursorref
    [0x257]@uvm.meta.close_cursor       (%cursor: framecursorref)

In all cases, ``cursor`` and ``stack`` cannot be ``NULL``.

- ``new_cursor`` allocates a frame cursor, referring to the top frame of
  ``%stack``. Returns the frame cursor reference.

- ``next_frame`` moves the frame cursor so that it refers to the frame below its
  current frame.

- ``copy_cursor`` allocates a frame cursor which refers to the same frame as
  ``%cursor``. Returns the frame cursor reference.

- ``close_cursor`` deallocates the cursor.

::

    [0x258]@uvm.meta.cur_func           (%cursor: framecursorref) -> int<32>
    [0x259]@uvm.meta.cur_func_Ver       (%cursor: framecursorref) -> int<32>
    [0x25a]@uvm.meta.cur_inst           (%cursor: framecursorref) -> int<32>
    [0x25b]@uvm.meta.dump_keepalives    (%cursor: framecursorref) -> @uvm.meta.refs.r

These functions operate on the frame referred by ``%cursor``. In all cases,
``%cursor`` cannot be ``NULL``.

- ``cur_func`` returns the ID of the frame. Returns 0 if the frame is native.

- ``cur_func_ver`` returns the ID of the current function version of the frame.
  Returns 0 if the frame is native, or the function of the frame is undefined.

- ``cur_inst`` returns the ID of the current instruction of the frame. Returns 0
  if the frame is just created, its function is undefined, or the frame is
  native.

- ``dump_keepalives`` dumps the values of the keep-alive variables of the
  current instruction in the frame. If the function is undefined, the arguments
  are the keep-alive variables. Cannot be used on native frames. The return
  value is a list of object references, each of which refers to an object which
  has type *T* and contains value *v*, where *T* and *v* are the type and the
  value of the corresponding keep-alive variable, respectively.

On-stack replacement
--------------------

::

    [0x25c]@uvm.meta.pop_frames_to (%cursor: framecursorref)
    [0x25d]@uvm.meta.push_frame <[sig]> (%cursor: framecursorref, %func: funcref<sig>)

``%cursor`` and ``%func`` must not be ``NULL``.

- ``pop_frames_to`` pops all frames above ``%cursor``.

- ``push_frame`` creates a new frame above the current frame pointed by the
  frame cursor ``%cursor``.  If there are frames above the current frame of
  ``%cursor``, they are popped.  The new frame is for the current version of
  function ``%func``.
  

Watchpoint operations
---------------------

::

    [0x25e]@uvm.meta.enable_watchpoint  (%wpid: int<32>)
    [0x25f]@uvm.meta.disable_watchpoint (%wpid: int<32>)

- ``enable_watchpoint``    enables  all watchpoints of watchpoint ID ``%wpid``.
- ``disenable_watchpoint`` disables all watchpoints of watchpoint ID ``%wpid``.

Trap handling
-------------

::

    [0x260]@uvm.meta.set_trap_handler (%handler: funcref<@uvm.meta.trap_handler.sig>, %userdata: ref<void>)

This instruction registers a trap handler. ``%handler`` is the function to be
called and ``%userdata`` will be their last argument when called.

This instruction overrides the trap handler registered via the C-based client
API.

A trap handler takes three parameters:

1. The stack where the trap takes place.
2. The watchpoint ID, or 0 if triggered by the ``TRAP`` instruction.
3. The user data, which is provided when registering.

A trap handler is run by the same Mu thread that caused the trap and is executed
on a new stack.

A trap handler *usually* terminates by either executing the ``@uvm.thread_exit``
instruction (probably also kill the old stack before exiting), or ``SWAPSTACK``
back to another stack while killing the stack the trap handler was running on.

Notes about dynamism
--------------------

These additional instructions are not dynamic. Unlike the C-based API, these
instructions do not use handles. Arguments, such as the additional arguments of
``push_frame`` are also statically typed. If the client needs dynamically typed
handles, it can always make its own. For example, ``push_frame`` can be wrapped
by a Mu function which takes a dynamic argument list, checks the argument types,
and executes a static ``@uvm.meta.push_frame`` instruction on the unboxed
values.

Some dynamic lookups, such as looking up constants by ID, are not available,
either. It can be worked around by maintaining a ``HashMap<id,value>`` (in the
form of Mu IR programs) which is updated with each bundle loading. In other
words, if the client does not maintain such a map, Mu will have to maintain it
for the client.

Dynamic Top-level Definition Lookup
-----------------------------------

::

    [0x268]@uvm.meta.constant_by_id <T> (%id: int<32>) -> T
    [0x269]@uvm.meta.global_by_id <T> (%id: int<32>) -> T
    [0x26a]@uvm.meta.func_by_id <T> (%id: int<32>) -> T
    [0x26b]@uvm.meta.expfunc_by_id <T> (%id: int<32>) -> T

These common instructions get the value of top-level definitions (i.e. global
SSA variables) by their IDs.  These functions are useful after a Mu IR bundle is
built at run time, and the IDs of the new top-level definitions are generated by
the IR builder.

``%id`` is the ID of the top-level definition.  ``T`` is the type of the
top-level definition.  Particularly, the type of a constant is the type
parameter of the constant definition; the type of a global cell is always
``iref<U>`` for some ``U``; the type of a function is always ``funcref<sig>``
for some ``sig``; the type of an exposed function is implementation-defined,
most likely ``ufuncptr<sig>`` for some ``sig``.  The return value is the same as
`the SSA variable of the top-level definition
<instruction-set#global-variable>`__.

- ``constant_by_id`` gets the value of a constant.

- ``global_by_id`` gets the internal reference of the global cell.

- ``func_by_id`` gets the function reference of the function.

- ``expfunc_by_id`` gets the implementation-dependent value of the exposed
  function.

IR Building Interface
---------------------

These common instructions are the counterpart of the `IR Builder API
<irbuilder.rst>`__.

The common instruction ``new_ir_builder`` will create an IR builder object, the
counterpart of the ``MuIRBuilder`` struct in the C API. Different from the C
API, common instructions refer to the IR builder object by the ``irbuilderref``
opaque reference type.

::

    [0x270]@uvm.irbuilder.new_ir_builder () -> irbuilderref

The canonical definition of each function is in the `IR Builder API
<irbuilder.rst>`__ chapter. The rules of mapping to comminsts are:

- The name of the common instruction is ``@uvm.irbuilder.`` followed by the name
  in the C API. Their IDs are allocated from 0x300 in the order they appear in
  the ``MuCtx`` struct, with ``new_bundle`` = 0x300.

- The first parameter ``MuIRBuilder`` is mapped to ``irbuilderref``.

- Other parameters are mapped to the rest of comminst parameters.

- ``MuID`` arguments are represented as the Mu type ``int<32>``, and
  ``MuCString`` (including ``MuName``) is mapped to ``iref<int<8>>``, which
  points to a ``'\0'``-terminated ASCII character array.

- Like the C API, the IR builder object referred by ``irbuilderref`` is
  invalidated after calling ``load`` or ``abort``.

- Array pointers become ``iref<T>``, where ``T`` is the element type.

- ``MuArraySize`` is always ``int<64>``.

- ``MuFlag`` parameters, including its aliases such as ``MuBinOptr`` take the
  same values as defined in the `<muapi.h>`__ header.

.. GEN:BEGIN:IRBUILDER_COMMINSTS

::

    [0x300]@uvm.irbuilder.load (%b: irbuilderref) -> ()
    [0x301]@uvm.irbuilder.abort (%b: irbuilderref) -> ()
    [0x302]@uvm.irbuilder.gen_sym (%b: irbuilderref, %name: iref<int<8>>) -> (int<32>)
    [0x303]@uvm.irbuilder.new_type_int (%b: irbuilderref, %id: int<32>, %len: int<32>) -> ()
    [0x304]@uvm.irbuilder.new_type_float (%b: irbuilderref, %id: int<32>) -> ()
    [0x305]@uvm.irbuilder.new_type_double (%b: irbuilderref, %id: int<32>) -> ()
    [0x306]@uvm.irbuilder.new_type_uptr (%b: irbuilderref, %id: int<32>, %ty: int<32>) -> ()
    [0x307]@uvm.irbuilder.new_type_ufuncptr (%b: irbuilderref, %id: int<32>, %sig: int<32>) -> ()
    [0x308]@uvm.irbuilder.new_type_struct (%b: irbuilderref, %id: int<32>, %fieldtys: iref<int<32>>, %nfieldtys: int<64>) -> ()
    [0x309]@uvm.irbuilder.new_type_hybrid (%b: irbuilderref, %id: int<32>, %fixedtys: iref<int<32>>, %nfixedtys: int<64>, %varty: int<32>) -> ()
    [0x30a]@uvm.irbuilder.new_type_array (%b: irbuilderref, %id: int<32>, %elemty: int<32>, %len: int<64>) -> ()
    [0x30b]@uvm.irbuilder.new_type_vector (%b: irbuilderref, %id: int<32>, %elemty: int<32>, %len: int<64>) -> ()
    [0x30c]@uvm.irbuilder.new_type_void (%b: irbuilderref, %id: int<32>) -> ()
    [0x30d]@uvm.irbuilder.new_type_ref (%b: irbuilderref, %id: int<32>, %ty: int<32>) -> ()
    [0x30e]@uvm.irbuilder.new_type_iref (%b: irbuilderref, %id: int<32>, %ty: int<32>) -> ()
    [0x30f]@uvm.irbuilder.new_type_weakref (%b: irbuilderref, %id: int<32>, %ty: int<32>) -> ()
    [0x310]@uvm.irbuilder.new_type_funcref (%b: irbuilderref, %id: int<32>, %sig: int<32>) -> ()
    [0x311]@uvm.irbuilder.new_type_tagref64 (%b: irbuilderref, %id: int<32>) -> ()
    [0x312]@uvm.irbuilder.new_type_threadref (%b: irbuilderref, %id: int<32>) -> ()
    [0x313]@uvm.irbuilder.new_type_stackref (%b: irbuilderref, %id: int<32>) -> ()
    [0x314]@uvm.irbuilder.new_type_framecursorref (%b: irbuilderref, %id: int<32>) -> ()
    [0x315]@uvm.irbuilder.new_type_irbuilderref (%b: irbuilderref, %id: int<32>) -> ()
    [0x316]@uvm.irbuilder.new_funcsig (%b: irbuilderref, %id: int<32>, %paramtys: iref<int<32>>, %nparamtys: int<64>, %rettys: iref<int<32>>, %nrettys: int<64>) -> ()
    [0x317]@uvm.irbuilder.new_const_int (%b: irbuilderref, %id: int<32>, %ty: int<32>, %value: int<64>) -> ()
    [0x318]@uvm.irbuilder.new_const_int_ex (%b: irbuilderref, %id: int<32>, %ty: int<32>, %values: iref<int<64>>, %nvalues: int<64>) -> ()
    [0x319]@uvm.irbuilder.new_const_float (%b: irbuilderref, %id: int<32>, %ty: int<32>, %value: float) -> ()
    [0x31a]@uvm.irbuilder.new_const_double (%b: irbuilderref, %id: int<32>, %ty: int<32>, %value: double) -> ()
    [0x31b]@uvm.irbuilder.new_const_null (%b: irbuilderref, %id: int<32>, %ty: int<32>) -> ()
    [0x31c]@uvm.irbuilder.new_const_seq (%b: irbuilderref, %id: int<32>, %ty: int<32>, %elems: iref<int<32>>, %nelems: int<64>) -> ()
    [0x31d]@uvm.irbuilder.new_const_extern (%b: irbuilderref, %id: int<32>, %ty: int<32>, %symbol: iref<int<8>>) -> ()
    [0x31e]@uvm.irbuilder.new_global_cell (%b: irbuilderref, %id: int<32>, %ty: int<32>) -> ()
    [0x31f]@uvm.irbuilder.new_func (%b: irbuilderref, %id: int<32>, %sig: int<32>) -> ()
    [0x320]@uvm.irbuilder.new_exp_func (%b: irbuilderref, %id: int<32>, %func: int<32>, %callconv: int<32>, %cookie: int<32>) -> ()
    [0x321]@uvm.irbuilder.new_func_ver (%b: irbuilderref, %id: int<32>, %func: int<32>, %bbs: iref<int<32>>, %nbbs: int<64>) -> ()
    [0x322]@uvm.irbuilder.new_bb (%b: irbuilderref, %id: int<32>, %nor_param_ids: iref<int<32>>, %nor_param_types: iref<int<32>>, %n_nor_params: int<64>, %exc_param_id: int<32>, %insts: iref<int<32>>, %ninsts: int<64>) -> ()
    [0x323]@uvm.irbuilder.new_dest_clause (%b: irbuilderref, %id: int<32>, %dest: int<32>, %vars: iref<int<32>>, %nvars: int<64>) -> ()
    [0x324]@uvm.irbuilder.new_exc_clause (%b: irbuilderref, %id: int<32>, %nor: int<32>, %exc: int<32>) -> ()
    [0x325]@uvm.irbuilder.new_keepalive_clause (%b: irbuilderref, %id: int<32>, %vars: iref<int<32>>, %nvars: int<64>) -> ()
    [0x326]@uvm.irbuilder.new_csc_ret_with (%b: irbuilderref, %id: int<32>, %rettys: iref<int<32>>, %nrettys: int<64>) -> ()
    [0x327]@uvm.irbuilder.new_csc_kill_old (%b: irbuilderref, %id: int<32>) -> ()
    [0x328]@uvm.irbuilder.new_nsc_pass_values (%b: irbuilderref, %id: int<32>, %tys: iref<int<32>>, %vars: iref<int<32>>, %ntysvars: int<64>) -> ()
    [0x329]@uvm.irbuilder.new_nsc_throw_exc (%b: irbuilderref, %id: int<32>, %exc: int<32>) -> ()
    [0x32a]@uvm.irbuilder.new_binop (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %optr: int<32>, %ty: int<32>, %opnd1: int<32>, %opnd2: int<32>, %exc_clause: int<32>) -> ()
    [0x32b]@uvm.irbuilder.new_binop_with_status (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %status_result_ids: iref<int<32>>, %n_status_result_ids: int<64>, %optr: int<32>, %status_flags: int<32>, %ty: int<32>, %opnd1: int<32>, %opnd2: int<32>, %exc_clause: int<32>) -> ()
    [0x32c]@uvm.irbuilder.new_cmp (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %optr: int<32>, %ty: int<32>, %opnd1: int<32>, %opnd2: int<32>) -> ()
    [0x32d]@uvm.irbuilder.new_conv (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %optr: int<32>, %from_ty: int<32>, %to_ty: int<32>, %opnd: int<32>) -> ()
    [0x32e]@uvm.irbuilder.new_select (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %cond_ty: int<32>, %opnd_ty: int<32>, %cond: int<32>, %if_true: int<32>, %if_false: int<32>) -> ()
    [0x32f]@uvm.irbuilder.new_branch (%b: irbuilderref, %id: int<32>, %dest: int<32>) -> ()
    [0x330]@uvm.irbuilder.new_branch2 (%b: irbuilderref, %id: int<32>, %cond: int<32>, %if_true: int<32>, %if_false: int<32>) -> ()
    [0x331]@uvm.irbuilder.new_switch (%b: irbuilderref, %id: int<32>, %opnd_ty: int<32>, %opnd: int<32>, %default_dest: int<32>, %cases: iref<int<32>>, %dests: iref<int<32>>, %ncasesdests: int<64>) -> ()
    [0x332]@uvm.irbuilder.new_call (%b: irbuilderref, %id: int<32>, %result_ids: iref<int<32>>, %n_result_ids: int<64>, %sig: int<32>, %callee: int<32>, %args: iref<int<32>>, %nargs: int<64>, %exc_clause: int<32>, %keepalive_clause: int<32>) -> ()
    [0x333]@uvm.irbuilder.new_tailcall (%b: irbuilderref, %id: int<32>, %sig: int<32>, %callee: int<32>, %args: iref<int<32>>, %nargs: int<64>) -> ()
    [0x334]@uvm.irbuilder.new_ret (%b: irbuilderref, %id: int<32>, %rvs: iref<int<32>>, %nrvs: int<64>) -> ()
    [0x335]@uvm.irbuilder.new_throw (%b: irbuilderref, %id: int<32>, %exc: int<32>) -> ()
    [0x336]@uvm.irbuilder.new_extractvalue (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %strty: int<32>, %index: int<32>, %opnd: int<32>) -> ()
    [0x337]@uvm.irbuilder.new_insertvalue (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %strty: int<32>, %index: int<32>, %opnd: int<32>, %newval: int<32>) -> ()
    [0x338]@uvm.irbuilder.new_extractelement (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %seqty: int<32>, %indty: int<32>, %opnd: int<32>, %index: int<32>) -> ()
    [0x339]@uvm.irbuilder.new_insertelement (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %seqty: int<32>, %indty: int<32>, %opnd: int<32>, %index: int<32>, %newval: int<32>) -> ()
    [0x33a]@uvm.irbuilder.new_shufflevector (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %vecty: int<32>, %maskty: int<32>, %vec1: int<32>, %vec2: int<32>, %mask: int<32>) -> ()
    [0x33b]@uvm.irbuilder.new_new (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %allocty: int<32>, %exc_clause: int<32>) -> ()
    [0x33c]@uvm.irbuilder.new_newhybrid (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %allocty: int<32>, %lenty: int<32>, %length: int<32>, %exc_clause: int<32>) -> ()
    [0x33d]@uvm.irbuilder.new_alloca (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %allocty: int<32>, %exc_clause: int<32>) -> ()
    [0x33e]@uvm.irbuilder.new_allocahybrid (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %allocty: int<32>, %lenty: int<32>, %length: int<32>, %exc_clause: int<32>) -> ()
    [0x33f]@uvm.irbuilder.new_getiref (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %refty: int<32>, %opnd: int<32>) -> ()
    [0x340]@uvm.irbuilder.new_getfieldiref (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %is_ptr: int<32>, %refty: int<32>, %index: int<32>, %opnd: int<32>) -> ()
    [0x341]@uvm.irbuilder.new_getelemiref (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %is_ptr: int<32>, %refty: int<32>, %indty: int<32>, %opnd: int<32>, %index: int<32>) -> ()
    [0x342]@uvm.irbuilder.new_shiftiref (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %is_ptr: int<32>, %refty: int<32>, %offty: int<32>, %opnd: int<32>, %offset: int<32>) -> ()
    [0x343]@uvm.irbuilder.new_getvarpartiref (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %is_ptr: int<32>, %refty: int<32>, %opnd: int<32>) -> ()
    [0x344]@uvm.irbuilder.new_load (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %is_ptr: int<32>, %ord: int<32>, %refty: int<32>, %loc: int<32>, %exc_clause: int<32>) -> ()
    [0x345]@uvm.irbuilder.new_store (%b: irbuilderref, %id: int<32>, %is_ptr: int<32>, %ord: int<32>, %refty: int<32>, %loc: int<32>, %newval: int<32>, %exc_clause: int<32>) -> ()
    [0x346]@uvm.irbuilder.new_cmpxchg (%b: irbuilderref, %id: int<32>, %value_result_id: int<32>, %succ_result_id: int<32>, %is_ptr: int<32>, %is_weak: int<32>, %ord_succ: int<32>, %ord_fail: int<32>, %refty: int<32>, %loc: int<32>, %expected: int<32>, %desired: int<32>, %exc_clause: int<32>) -> ()
    [0x347]@uvm.irbuilder.new_atomicrmw (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %is_ptr: int<32>, %ord: int<32>, %optr: int<32>, %ref_ty: int<32>, %loc: int<32>, %opnd: int<32>, %exc_clause: int<32>) -> ()
    [0x348]@uvm.irbuilder.new_fence (%b: irbuilderref, %id: int<32>, %ord: int<32>) -> ()
    [0x349]@uvm.irbuilder.new_trap (%b: irbuilderref, %id: int<32>, %result_ids: iref<int<32>>, %rettys: iref<int<32>>, %nretvals: int<64>, %exc_clause: int<32>, %keepalive_clause: int<32>) -> ()
    [0x34a]@uvm.irbuilder.new_watchpoint (%b: irbuilderref, %id: int<32>, %wpid: int<32>, %result_ids: iref<int<32>>, %rettys: iref<int<32>>, %nretvals: int<64>, %dis: int<32>, %ena: int<32>, %exc: int<32>, %keepalive_clause: int<32>) -> ()
    [0x34b]@uvm.irbuilder.new_wpbranch (%b: irbuilderref, %id: int<32>, %wpid: int<32>, %dis: int<32>, %ena: int<32>) -> ()
    [0x34c]@uvm.irbuilder.new_ccall (%b: irbuilderref, %id: int<32>, %result_ids: iref<int<32>>, %n_result_ids: int<64>, %callconv: int<32>, %callee_ty: int<32>, %sig: int<32>, %callee: int<32>, %args: iref<int<32>>, %nargs: int<64>, %exc_clause: int<32>, %keepalive_clause: int<32>) -> ()
    [0x34d]@uvm.irbuilder.new_newthread (%b: irbuilderref, %id: int<32>, %result_id: int<32>, %stack: int<32>, %threadlocal: int<32>, %new_stack_clause: int<32>, %exc_clause: int<32>) -> ()
    [0x34e]@uvm.irbuilder.new_swapstack (%b: irbuilderref, %id: int<32>, %result_ids: iref<int<32>>, %n_result_ids: int<64>, %swappee: int<32>, %cur_stack_clause: int<32>, %new_stack_clause: int<32>, %exc_clause: int<32>, %keepalive_clause: int<32>) -> ()
    [0x34f]@uvm.irbuilder.new_comminst (%b: irbuilderref, %id: int<32>, %result_ids: iref<int<32>>, %n_result_ids: int<64>, %opcode: int<32>, %flags: iref<int<32>>, %nflags: int<64>, %tys: iref<int<32>>, %ntys: int<64>, %sigs: iref<int<32>>, %nsigs: int<64>, %args: iref<int<32>>, %nargs: int<64>, %exc_clause: int<32>, %keepalive_clause: int<32>) -> ()

.. GEN:END:IRBUILDER_COMMINSTS

.. vim: tw=80
